defmodule AirtableTest do
  use PowerAssert
  doctest Airtable

  test "get all rows for table_red records" do
    assert Airtable.get_rows("table_red") == [%{"Name" => "Second element"}, %{"Name" => "First element"}]
  end

  test "retrieve table_red record by id" do
    assert Airtable.get_row_by_id("table_red", "recoSw6T23ufm0D6h") == %{"Name" => "First element"}
  end

  test "embed blue table with green table by table_green column " do
    blue_rows = Airtable.get_rows("table_blue")
    result = [%{"Name" => "Blue 2", "table_green" => [%{"Name" => "Green 1", "table_blue" => ["rechAIonHij7aRqb9", "recGmBWzTS9I5aaNj"]}]},
            %{"Name" => "Blue 1",
              "table_green" => [%{"Name" => "Green 1", "table_blue" => ["rechAIonHij7aRqb9", "recGmBWzTS9I5aaNj"]},
               %{"Name" => "Green 2", "table_blue" => ["rechAIonHij7aRqb9"]}]}]

    assert Airtable.embed_by(blue_rows, "table_green", "table_green") == result
  end
end
