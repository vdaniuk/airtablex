defmodule Airtable do
  import Enum, only: [map: 2]
  @api_key Application.get_env(:airtable, :api_key)

  def list_records(table) do
     gen_table_string(table) <> "?api_key=#{@api_key}"
     |> HTTPoison.get
     |> handle_response
  end

  def fetch_rows(table) do
    table
    |> list_records
    |> extract_fields # for cleaning out metadata
    |> Enum.filter( &(!Enum.empty?(&1)) ) #filtering out existing empty rows
  end

  def fetch_row_by_id(table, record_id) do
    gen_table_string(table) <>  "/" <> record_id <> "?api_key=#{@api_key}"
    |> HTTPoison.get
    |> handle_response
    |> extract_fields
  end


  @doc """
  TODO: fetches all table data for embedding, may be slow for large tables
  eagerly loads all data from linked rows from {table} for records, finding matches by {column}
  """
  @spec embed_by(list, String.t, String.t) :: list
  def embed_by(records,_,_) when is_list(records) == false, do:   throw "embed_by records param isn't a list"

  def embed_by(records, fkey_column_name, embeds_table_name) do
    %{"records" => records_to_embed} = list_records(embeds_table_name)

    records
    |> Enum.filter( fn %{^fkey_column_name => f} -> is_list(f) end ) # filters out rows with empty fkey cells
    |> Enum.map( fn record ->
      embeds = for fkey <- record[fkey_column_name], do: find_row_by_pkey(fkey, records_to_embed)
      put_in record[fkey_column_name], embeds
    end)
  end

  def find_row_by_pkey( records, id) when is_list(records) and is_bitstring(id) do
    records
    |> Enum.find( fn %{"id" => x} -> x == id end)
    |> Map.get("fields")
  end

  def find_row_by( records, column, value) when is_list(records) do
    eq = fn
      %{^column => v} -> v == value
      _ -> false
    end
    records
    |> Enum.find(eq)
    |> Map.get("fields")
  end

  defp gen_table_string(table) do
      airtable_base = Application.get_env(:airtable, :base)
     "https://api.airtable.com/v0/#{airtable_base}/#{table}"
  end

  defp extract_fields(%{"records" => records}) when is_list(records), do: Enum.map records, &extract_fields/1
  defp extract_fields(%{"fields" => fields}), do: fields

  defp handle_response(response) do
    case response do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Poison.decode!(body)
      {:ok, %HTTPoison.Response{status_code: status_code}} ->
        raise "Airtablex Error: Airtable API responded with status code #{status_code}"
      {:error, %HTTPoison.Error{reason: reason}} ->
        raise "Airtablex Error: HTTP " <> reason
      end
  end



end
